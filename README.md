# Individual Repository for DevOps

This repository contains the files for all the class assignemts of DevOps.

* [Class Assignment 1](https://bitbucket.org/atb/devops-19-20-rep-template/src/master/ca1/)

* [Class Assignment 2](https://bitbucket.org/atb/devops-19-20-rep-template/src/master/ca2/)

* [Class Assignment 3](https://bitbucket.org/atb/devops-19-20-rep-template/src/master/ca3/)

* [Class Assignment 4](https://bitbucket.org/atb/devops-19-20-rep-template/src/master/ca4/)

* [Class Assignment 5](https://bitbucket.org/atb/devops-19-20-rep-template/src/master/ca5/)

* [Class Assignment 6](https://bitbucket.org/atb/devops-19-20-rep-template/src/master/ca6/)

**Note**
You should use Markdown Syntax so that the contents are properly rendered in Bitbucket. See how to write README files for Bitbucket in [here](https://confluence.atlassian.com/bitbucket/readme-content-221449772.html)
